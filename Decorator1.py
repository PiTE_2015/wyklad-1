__author__ = 'ja'


def debug(func):
    msg = func.__qualname__

    def wrapper(*args, **kwargs):
        print(msg)
        return func(*args, **kwargs)
    return wrapper


def sum(x, y):
    return x + y


def diff(x, y):
    return x - y


def mul(x,y):
    return x * y

if __name__ == "__main__":
    sum=debug(sum)
    print (sum(1,2))

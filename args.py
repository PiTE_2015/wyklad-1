def foo(*args,**kwargs):
    print("***args***")
    print(args)
    print("***kwargs***")
    print(kwargs)


def sum(*args, initial=0):
    print("***sum****")
    result = initial
    for arg in args:
        result = result + arg
    return result

if __name__ == "__main__":
    foo(1,2,pi=3.15)
    print(sum(1,2,3,initial=5))
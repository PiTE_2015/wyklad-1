def startAt(x):
   def incrementBy(y):
       return x + y
   return incrementBy

if __name__ == "__main__":
    move= startAt(10)
    print(move(1))
    print(move(2))


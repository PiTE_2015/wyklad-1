__author__ = 'ja'


class CounterExceededException(Exception):
    def __init__(self, msg):
        self.message = msg


class Counter:
    def __init__(self):
        self.max=3

    def countdown(self,limit):
        for count in range(limit):
            print(count)
            if count > self.max:
                raise CounterExceededException(count)

if __name__ == "__main__":
    counter=Counter()
    counter.countdown(100)

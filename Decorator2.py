__author__ = 'ja'


def debug(func):
    msg = func.__qualname__

    def wrapper(*args, **kwargs):
        print(msg)
        return func(*args, **kwargs)
    return wrapper

@debug
def sum(x, y):
    return x + y

if __name__ == "__main__":
    print (sum(1,2))

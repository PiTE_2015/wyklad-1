class Spam:
    a = 1
    def __init__(self, b):
        self.b = b

    def iMethod(self):
        print("Instance Method "+str(self.b))

    @classmethod
    def cmethod(cls):
        print("Class variable:" + str(Spam.a))


    @staticmethod
    def smethod():
        print ("Static Method")

if __name__ == "__main__":
    spam=Spam(10)
    spam.iMethod()
    Spam.cmethod()
    Spam.smethod()

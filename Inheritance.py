__author__ = 'ja'

class Polygon(object):
    def __init__(self, id):
        self.id = id
    def __repr__(self):
        return "ID: "+str(self.id)

class Rectangle(Polygon):
    def __init__(self, id, width, height):
        super(self.__class__, self).__init__(id)
        self.width = width
        self.height=height

    def __repr__(self):
        output=super().__repr__()
        output += " width " +str(self.width) + " height " + str(self.height)
        return output

if __name__ == "__main__":
    rectangle=Rectangle(1,100,10)
    print(rectangle)
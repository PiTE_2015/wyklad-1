__author__ = 'ja'

class A:
  def call(self):
    pass

class B1(A):
  def call(self):
    print("I am parent B1")

class B2(A):
  def call(self):
    print("I am parent B2")

class B3(A):
  def call(self):
    print("I am parent B3")

class C(A):
  def call(self):
    print("I (C) was not invited")

class ME(B2, B1, B3):
    def whichCall(self):
        return self.call()

if __name__ == "__main__":
    me=ME()
    me.whichCall()